/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos; 

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author usr
 */
public class BaseDatos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BaseDatos bd = new BaseDatos();
        bd.conectar();
        ActorDAO a1=new ActorDAO();
      
        System.out.println(a1.selectActor(30));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
/*
        try {
            a1.insertar(new Actor("Pepe","Pecas",201,format.parse("2000-11-27")));
        } catch (ParseException ex) {
            Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
  */      a1.delete_id(214);
        ArrayList<Actor> actores = a1.selectAll(); 
    }
    
    public void conectar() {
        //Definimos el conector a la base de datos
        String driver = "com.mysql.jdbc.Driver";
        // El nombre de la base de datos
        String database = "sakila";
        // El usuario 
        String user = "root";
        // La contraseña
        String password = "nada";
        // El puerto de conexion
        String port = "3306";
        // El servidor
        String server = "localhost";
        // La url de conexion
        // jdbc:mysql://localhost:3306/sakila
        String url = "jdbc:mysql://"+server+":"+port
                +"/"+database;
        
        Connection con = null;
        
        try{
            Class.forName(driver);
            con = (Connection) DriverManager.getConnection(url, user, password);
            System.out.println("Conexion Exitosa");
        } catch (ClassNotFoundException ex) {
            System.out.println("No se encontró el conector");
            //Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.out.println("No se puede conectar a la base de datos");
            //Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            // Cerramos la conexion a la base de datos
            if(con != null){
                try {
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        } 
        
    }
    
}
