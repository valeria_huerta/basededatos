/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usr
 */
public class Conexion {
    //Definimos el conector a la base de datos
    // com.postgresql.jdbc.Driver
    String driver = "com.mysql.jdbc.Driver";    
    // El nombre de la base de datos
    String database = "sakila";
    // El usuario 
    String user = "root";
    // La contraseña
    String password = "nada";
    // El puerto de conexion
    String port = "3306";
    // El servidor
    String server = "localhost";
    // La url de conexion
    // jdbc:mysql://localhost:3306/sakila
    // jdbc:postgrest
    String url = "jdbc:mysql://"+server+":"+port
            +"/"+database;
    Connection con = null;
    
    public Connection conectar() {
        try{
            Class.forName(driver);
            con = (Connection) DriverManager.getConnection(url, user, password);
            return con;
        } catch (ClassNotFoundException ex) {
            System.out.println("No se encontró el conector");
            //Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.out.println("No se puede conectar a la base de datos");
            //Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void desconectar(){
        if(con != null){
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
}
