/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author usr
 */
public class Actor {
    private Integer id;
    private String firstName;
    private String lastName;
    private Date lastUpdate;   
    Actor(){
        
    }
    Actor(String firstName,String lastName, Integer id, Date lastUpdate){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setId(id);
        this.setLastUpdate(lastUpdate);
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the lastUpdate
     */
    public Date getLastUpdate() {
        return lastUpdate;
    }

    /**
     * @param lastUpdate the lastUpdate to set
     */
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }    
    
    /**
     *
     */
    @Override
    public String toString(){
        return(id + "," + firstName + "," + lastName + "," + lastUpdate);
    }
    
}
