/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author usr
 */
public class ActorDAO {
    Conexion con = new Conexion();
    
    public Actor selectActor(Integer id){
        Actor actor = new Actor();
        
        try {            
            String sql = "SELECT * FROM actor WHERE "
                    + "actor_id = ?";
            
            PreparedStatement stmt =null;
            stmt = con.conectar().prepareStatement(sql);
            stmt.setInt(1, id);            
            
            ResultSet rs = stmt.executeQuery();
            
            if(rs.next()!=false){
                actor.setId(rs.getInt("actor_id"));
                actor.setFirstName(rs.getString("first_name"));
                actor.setLastName(rs.getString("last_name"));
                actor.setLastUpdate(rs.getDate("last_update"));               
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ActorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            con.desconectar();
            return actor;
        }        
    }
    
    
    public ArrayList<Actor> selectAll(){
        ArrayList<Actor> actores = new ArrayList();
        try {            
            String sql = "SELECT * FROM actor";
            PreparedStatement stmt =null;
            stmt = con.conectar().prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();            
            while(rs.next()!=false){
                Actor actor = new Actor();
                actor.setId(rs.getInt("actor_id"));
                actor.setFirstName(rs.getString("first_name"));
                actor.setLastName(rs.getString("last_name"));
                actor.setLastUpdate(rs.getDate("last_update"));
                actores.add(actor);
                System.out.println(actor);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(ActorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            con.desconectar();
            return actores;
        }        
    }
    
    public boolean insertar(Actor actor){
        try {            
            String sql = "INSERT INTO actor(first_name, last_name, last_update) "
                    + "VALUES(?,?,?);";              
            PreparedStatement stmt =null;
            stmt = con.conectar().prepareStatement(sql);
            stmt.setString(1, actor.getFirstName());
            stmt.setString(2, actor.getLastName());
            SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dmy = dmyFormat.format(actor.getLastUpdate());
            stmt.setDate(3, Date.valueOf( dmy));
      

            return stmt.execute();            
            
        } catch (SQLException ex) {
            Logger.getLogger(ActorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            con.desconectar();            
        }        
        return false;
    }
        public void delete_id( int id){
        try {
            String sql = "DELETE FROM Actor WHERE actor_id = ?";
          
            PreparedStatement stmt =null;
            stmt =  con.conectar().prepareStatement(sql);
            stmt.setInt(1, id);
            stmt.execute();
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
